<?php

function taxonomy_expire_admin_page(){
  $options = taxonomy_get_vocabularies();
  foreach ($options as $option){
    $vocabulabies[$option->vid] =  $option->name;
  }
  $form['taxonomy_expire_vocabularies'] = array(
    '#type'=>'checkboxes',
    '#options' => $vocabulabies,
    '#default_value' => variable_get('taxonomy_expire_vocabularies', array('')),
    '#description' => t('Choose which vocabulary to use expire usage'),
  );
  return system_settings_form($form);
}
 
